#!/bin/sh

alias tsudo='printf "%s" "$SUDO_PASSWORD" | sudo -Skp ""'

echo_debug() {                                               # DEBUG
    printf '\033[1mDEBUG:\033[0m \033[2m%s...\033[0m\n' "$1" # DEBUG
}                                                            # DEBUG

urlencode() {
    # This is a false positive
    # shellcheck disable=SC1083
    ENCODEDURL="$(curl -Gs -w %{url_effective} --data-urlencode @- ./ ||: )"
    printf '%s' "$ENCODEDURL" | sed 's/%0[aA]$//;s/^.*[?]//'
}

get_asp_var() {
    i=0
    for VAR in __VIEWSTATE __VIEWSTATEGENERATOR __EVENTVALIDATION ; do
        VAL="$(printf '%s' "$RESPONSE" | grep "id=\"$VAR\"" | cut -d '"' -f 8 | urlencode)"
        [ "$i" != 0 ] && printf '&'
        printf '%s=%s' "$VAR" "$VAL"
        i=+1
    done
}

get_archive() {
    URL="https://soportefirmadigital.com/sfdj/dl.aspx"
    VERSION="$(printf '%s' "$VERSION" | urlencode)"
    RESPONSE="$(curl -s --compressed "$URL" -o -)"
    ASP_VARS="$(get_asp_var)"

    FILE="$(curl -s "$URL" --data-raw "$ASP_VARS" --data-raw "ctl00%24certContents%24ddlInstaladores=$VERSION" |
        grep 'hiddenISO.*value="' | sed 's/^.*value="//g;s/".*$//g')"
    [ -z "$FILE" ] && return 1

    TEMPKEY="$(curl -s --compressed "$URL" --data-raw "$ASP_VARS" \
        --data-raw "__EVENTTARGET=ctl00%24certContents%24LinkButton3" \
        --data-raw "ctl00%24certContents%24hiddenISO=$FILE" \
        --data-raw "ctl00%24certContents%24txtSerialNumber=$SERIAL" \
        --data-raw "ctl00%24certContents%24chkConfirmo=on" \
        -o - | sed '/tempkey/!d;s/.*tempkey=//g;s/".*$//g')"
    [ -z "$TEMPKEY" ] && return 1

    printf '%s %s' "$FILE" "$TEMPKEY"
}

set_version() {
    # This is a shellcheck limitation
    # shellcheck disable=SC1090
    for os in /etc/os-release /usr/lib/os-release; do
        [ -f $os ] && . $os && break
    done

    case "$ID" in
        centos) [ -n "$VERSION_ID" ] && [ "$VERSION_ID" -ge 9 ] &&
                          ID="fedora" ;;
        *suse*|sles|sled) ID="suse" ;;
        debian|ubuntu)    ID="debian" ;;
        fedora|rhel)      ID="fedora" ;;
        arch|manjaro)     ID="arch" ;;
        *)                ID="${ID_LIKE%% *}" ;;
    esac

    [ "$ID" = "ubuntu" ] && ID="debian"
    [ "$ID" = "rhel" ]   && ID="fedora"
    [ -f /System/Library/CoreServices/SystemVersion.plist ] && ID="macos"

    case "$ID" in
        debian) VERSION="Usuarios Linux (DEB 64bits)" ;;
        fedora) VERSION="Usuarios Linux (RPM 64bits)" ;;
        suse)   VERSION="Usuarios Linux (RPM 64bits)" ;;
        arch)   VERSION="Usuarios Linux (RPM 64bits)" ;;
        centos) VERSION="Usuarios Linux RPM (CentOS 7)" ;;
        macos)  VERSION="Usuarios MAC" ;;
        *) return 1 ;;
    esac
}

set_menu() {
if [ -z "$MENU" ] ; then
    command -v zenity > /dev/null && MENU="zenity" && return 0
    command -v kdialog > /dev/null && MENU="kdialog" && return 0
    MENU="term"
fi
}

menu() {
MODE="$1" PROMPT="$2"

if [ "$MENU" = "zenity" ] ; then
    echo_debug "MENU: $MENU MODE: $MODE PROMPT: $PROMPT" > /dev/stderr # DEBUG

    [ "$MODE" = "info" ] &&
        zenity --title "$TITLE" --text "$PROMPT" --info

    [ "$MODE" = "error" ] &&
        zenity --title "$TITLE" --text "$PROMPT" --error > /dev/stderr

    [ "$MODE" = "entry" ] &&
        zenity --title "$TITLE" --text "$PROMPT" --entry

    [ "$MODE" = "pass" ] &&
        zenity --title "$TITLE" --password

elif [ "$MENU" = "term" ] ; then
    # nil (or whatever variable) must be set for portability
    # shellcheck disable=SC2034
    [ "$MODE" = "info" ]  &&
        printf '\n\033[1m\033[34m=== %s ===\033[0m\n%s ENTER' "$TITLE" "$PROMPT" >/dev/stdin &&
        read -r nil

    [ "$MODE" = "error" ] &&
        printf '\n\033[1m\033[31m=== %s ===\033[0m\n%s\n'     "$TITLE" "$PROMPT" >/dev/stderr

    [ "$MODE" = "entry" ] &&
        printf '\n\033[1m\033[34m=== %s ===\033[0m\n%s\n -> ' "$TITLE" "$PROMPT" >/dev/stdin &&
        IFS= read -r entry &&
        printf '%s' "$entry"

    [ "$MODE" = "pass" ] &&
        printf '\n\033[1m\033[34m=== %s ===\033[0m\n%s\n -> ' "$TITLE" "$PROMPT" >/dev/stdin &&
        IFS= read -r passwd &&
        printf '%s' "$passwd"

fi

}
