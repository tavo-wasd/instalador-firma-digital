SCRIPT=instalador-certificados.sh

all: $(SCRIPT)

$(SCRIPT): $(wildcard [0-9][0-9]-*.sh)
	printf '#!/bin/sh\n' > $@
	for i in $? ; do cat $$i ; done | sed '/^ *# shellcheck/! s/^ *#.*$$//g;/.*# *DEBUG/d' >> $@

debug: $(wildcard [0-9][0-9]-*.sh)
	printf '#!/bin/sh\n' > $@-$(SCRIPT)
	for i in $? ; do cat $$i ; done >> $@-$(SCRIPT)

clean:
	rm -rf debug-$(SCRIPT) $(SCRIPT)
