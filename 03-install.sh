#!/bin/sh

debian_install_certs() {
# Source: https://fran.cr/instalar-firma-digital-costa-rica-gnu-linux-ubuntu-debian/

echo_debug "Instalando dependencias" # DEBUG
tsudo apt-get install -y unzip binutils p11-kit pcscd bubblewrap icedtea-netx > /dev/null || return 1

echo_debug "Extraer fichero" # DEBUG
[ -z "$SAVE_DIR" ] || [ -z "$SAVE_FILE" ] && return 1
(cd "$SAVE_DIR" && unzip -u "$SAVE_FILE" > /dev/null) || return 1

echo_debug "Copiar certificados" # DEBUG
for cert in "$(find "$SAVE_DIR" -name "Certificados")"/* ; do
    certname="${cert##*/}"
    tsudo cp "$cert" /usr/local/share/ca-certificates/"${certname%.cer}.crt"
done

echo_debug "Extraer módulo privativo" #DEBUG
PACKAGE="$(find "$SAVE_DIR" -name "idprotectclient[-_]*.deb")"
PACKAGE_DIR="${PACKAGE%/*}"
PACKAGE="${PACKAGE##*/}"
[ -z "$PACKAGE_DIR" ] || [ -z "$PACKAGE" ] && return 1
(cd "$PACKAGE_DIR" && ar p "$PACKAGE" data.tar.gz | tar zx ./usr/lib/x64-athena/libASEP11.so)
tsudo cp -p "$PACKAGE_DIR"/usr/lib/x64-athena/libASEP11.so /usr/lib/x86_64-linux-gnu/

echo_debug "Symlinks y componentes..." # DEBUG
# This must not be expanded
# shellcheck disable=SC2016
tsudo sh -c '
# --- Certificados ---
for file in /usr/local/share/ca-certificates/*.crt ; do openssl x509 -inform DER -in "$file" -out "$file.tmp" 2> /dev/null ; done
find /usr/local/share/ca-certificates/ -type f -empty -delete
for i in /usr/local/share/ca-certificates/*.tmp ; do mv "$i" "${i%.tmp}" ; done
update-ca-certificates --fresh > /dev/null
# --- Instalación del módulo PKCS#11 ---
mkdir -p /usr/lib/x64-athena
mkdir -p /Firma_Digital/LIBRERIAS
ln -sf /usr/lib/x86_64-linux-gnu/libASEP11.so /usr/lib/x64-athena/
ln -sf /usr/lib/x86_64-linux-gnu/libASEP11.so /usr/lib/
ln -sf /usr/lib/x86_64-linux-gnu/libASEP11.so /usr/local/lib/
ln -sf /usr/lib/x86_64-linux-gnu/libASEP11.so /Firma_Digital/LIBRERIAS/
ln -sf /usr/local/share/ca-certificates /Firma_Digital/CERTIFICADOS
systemctl enable --now pcscd.socket > /dev/null
'

echo_debug "Configurando IDPClientDB" #DEBUG
tsudo sh -c "
mkdir -p /etc/Athena
echo \"<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<IDProtect>
 <TokenLibs>
  <IDProtect>
   <Cards>
    <IDProtectXF>
     <ATR type='hexBinary'>3BDC00FF8091FE1FC38073C821106600000000000000</ATR>
     <ATRMask type='hexBinary'>FFFF00FFF0FFFFFFFFFFFFFFFFF0FF00000000000000</ATRMask>
    </IDProtectXF>
   </Cards>
  </IDProtect>
  <ChipDoc>
   <Cards>
    <ChipDocEMV>
     <ATR type='hexBinary'>3BEA00008131FE450031C173C840000090007A</ATR>
     <ATRMask type='hexBinary'>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF</ATRMask>
    </ChipDocEMV>
   </Cards>
  </ChipDoc>
 </TokenLibs>
</IDProtect>\" > /etc/Athena/IDPClientDB.xml
"

echo_debug "Configurando p11-kit/modules" #DEBUG
tsudo sh -c "
mkdir -p /usr/share/p11-kit/modules
echo 'remote: |bwrap --unshare-all --dir /tmp --ro-bind /etc/Athena /etc/Athena --proc /proc --dev /dev --ro-bind /usr /usr --ro-bind /lib /lib --ro-bind /lib64 /lib64 --ro-bind /var/run/pcscd /var/run/pcscd --ro-bind /run/pcscd /run/pcscd p11-kit remote /usr/lib/x86_64-linux-gnu/libASEP11.so' > /usr/share/p11-kit/modules/firma-digital.module
"

echo_debug "Configurando p11-kit update symlinks" # DEBUG
tsudo sh -c "
mkdir -p /usr/local/sbin
echo \"#!/bin/sh

FIREFOX_LIB=/usr/lib/firefox/libnssckbi.so
FIREFOX_ESR_LIB=/usr/lib/firefox-esr/libnssckbi.so
THUNDERBIRD_LIB=/usr/lib/thunderbird/libnssckbi.so
NSS_LIB=/usr/lib/x86_64-linux-gnu/nss/libnssckbi.so

if [ -e \"\$FIREFOX_LIB\" ]
then
    if ! [ -L \"\$FIREFOX_LIB\" ]
    then
        echo \"Firefox libnssckbi.so is not a symlink. Fixing...\"
        mv -f \"\$FIREFOX_LIB\" \"\$FIREFOX_LIB\".bak
        ln -s /usr/lib/x86_64-linux-gnu/p11-kit-proxy.so \"\$FIREFOX_LIB\"
    fi
fi

if [ -e \"\$FIREFOX_ESR_LIB\" ]
then
    if ! [ -L \"\$FIREFOX_ESR_LIB\" ]
    then
        echo \"Firefox ESR libnssckbi.so is not a symlink. Fixing...\"
        mv -f \"\$FIREFOX_ESR_LIB\" \"\$FIREFOX_ESR_LIB\".bak
        ln -s /usr/lib/x86_64-linux-gnu/p11-kit-proxy.so \"\$FIREFOX_ESR_LIB\"
    fi
fi

if [ -e \"\$THUNDERBIRD_LIB\" ]
then
    if ! [ -L \"\$THUNDERBIRD_LIB\" ]
    then
        echo \"Thunderbird libnssckbi.so is not a symlink. Fixing...\"
        mv -f \"\$THUNDERBIRD_LIB\" \"\$THUNDERBIRD_LIB\".bak
        ln -s /usr/lib/x86_64-linux-gnu/p11-kit-proxy.so \"\$THUNDERBIRD_LIB\"
    fi
fi

if [ -e \"\$NSS_LIB\" ]
then
    if ! [ -L \"\$NSS_LIB\" ]
    then
        echo \"NSS libnssckbi.so is not a symlink. Fixing...\"
        mv -f \"\$NSS_LIB\" \"\$NSS_LIB\".bak
        ln -s /usr/lib/x86_64-linux-gnu/p11-kit-proxy.so \"\$NSS_LIB\"
    fi
fi\" > /usr/local/sbin/update-p11-kit-symlinks
chmod +x /usr/local/sbin/update-p11-kit-symlinks
"

echo_debug "Configurando módulo mantenimiento systemd" # DEBUG
tsudo sh -c "
mkdir -p /etc/systemd/system
echo \"[Unit]
Description=mantenimiento de enlaces a p11-kit-proxy

[Service]
Type=oneshot
ExecStart=/usr/local/sbin/update-p11-kit-symlinks

[Install]
WantedBy=multi-user.target
\" > /etc/systemd/system/p11-kit-proxy-updater.service
systemctl enable --now p11-kit-proxy-updater.service > /dev/null
"

echo_debug "Instalando trust module pk11" # DEBUG
tsudo sh -c "
mkdir -p /etc/pkcs11/modules
echo 'disable-in:' > /etc/pkcs11/modules/p11-kit-trust.module
"
}

fedora_install_certs() {
# Source: https://fran.cr/instalar-firma-digital-costa-rica-gnu-linux-fedora/

echo_debug "Instalando dependencias" # DEBUG
tsudo dnf -y install unzip pcsc-lite icedtea-web > /dev/null || return 1

echo_debug "Extraer fichero" # DEBUG
[ -z "$SAVE_DIR" ] || [ -z "$SAVE_FILE" ] && return 1
(cd "$SAVE_DIR" && unzip -u "$SAVE_FILE" > /dev/null) || return 1

echo_debug "Copiar certificados" # DEBUG
tsudo cp -p "$(find "$SAVE_DIR" -name "Certificados")"/* /usr/share/pki/ca-trust-source/anchors/
tsudo update-ca-trust

echo_debug "Extraer módulo privativo" # DEBUG
PACKAGE="$(find "$SAVE_DIR" -name "idprotectclient[-_]*.rpm")"
PACKAGE_DIR="${PACKAGE%/*}"
PACKAGE="${PACKAGE##*/}"
[ -z "$PACKAGE_DIR" ] || [ -z "$PACKAGE" ] && return 1
(cd "$PACKAGE_DIR" &&
	rm -r ./usr/lib/x64-athena/libASEP11.so
	rpm2cpio "$PACKAGE" | cpio -dim ./usr/lib/x64-athena/libASEP11.so) || return 1
tsudo cp -p "$PACKAGE_DIR"/usr/lib/x64-athena/libASEP11.so /usr/lib64/

echo_debug "Symlinks y componentes..." # DEBUG
tsudo sh -c '
mkdir -p /usr/lib/x64-athena/
mkdir -p /Firma_Digital/LIBRERIAS/
ln -sf /usr/lib64/libASEP11.so /usr/lib/x64-athena/
ln -sf /usr/lib64/libASEP11.so /usr/lib/
ln -sf /usr/lib64/libASEP11.so /usr/local/lib/
ln -sf /usr/lib64/libASEP11.so /Firma_Digital/LIBRERIAS/
ln -sf /usr/share/pki/ca-trust-source/anchors /Firma_Digital/CERTIFICADOS
systemctl enable --now pcscd.socket > /dev/null
'

echo_debug "Configurando IDPClientDB" # DEBUG
tsudo sh -c "
mkdir -p /etc/Athena
echo \"<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<IDProtect>
 <TokenLibs>
  <IDProtect>
   <Cards>
    <IDProtectXF>
     <ATR type='hexBinary'>3BDC00FF8091FE1FC38073C821106600000000000000</ATR>
     <ATRMask type='hexBinary'>FFFF00FFF0FFFFFFFFFFFFFFFFF0FF00000000000000</ATRMask>
    </IDProtectXF>
   </Cards>
  </IDProtect>
  <ChipDoc>
   <Cards>
    <ChipDocEMV>
     <ATR type='hexBinary'>3BEA00008131FE450031C173C840000090007A</ATR>
     <ATRMask type='hexBinary'>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF</ATRMask>
    </ChipDocEMV>
   </Cards>
  </ChipDoc>
 </TokenLibs>
</IDProtect>\" > /etc/Athena/IDPClientDB.xml
"

echo_debug "Configurando p11-kit/modules" # DEBUG
tsudo sh -c "
mkdir -p /usr/share/p11-kit/modules
echo 'remote: |bwrap --unshare-all --dir /tmp --proc /proc --dev /dev --ro-bind /etc/Athena /etc/Athena --ro-bind /usr /usr --ro-bind /var/run/pcscd /var/run/pcscd --ro-bind /run/pcscd /run/pcscd --symlink /usr/lib64 /lib64 p11-kit remote /usr/lib64/libASEP11.so' > /usr/share/p11-kit/modules/firma-digital.module
"
}

arch_install_certs() {
echo_debug "Instalando dependencias" # DEBUG
tsudo pacman -S --noconfirm --needed unzip cpio rpm-tools pcsclite ccid jre8-openjdk icedtea-web > /dev/null || return 1

echo_debug "Extraer fichero" # DEBUG
[ -z "$SAVE_DIR" ] || [ -z "$SAVE_FILE" ] && return 1
(cd "$SAVE_DIR" && unzip -u "$SAVE_FILE" > /dev/null)

echo_debug "Copiar certificados" # DEBUG
tsudo cp -p "$(find "$SAVE_DIR" -name "Certificados")"/* /usr/share/ca-certificates/trust-source/anchors/
tsudo update-ca-trust

echo_debug "Extraer módulo privativo" # DEBUG
[ -z "$PACKAGE_DIR" ] || [ -z "$PACKAGE" ] && return 1
(cd "$PACKAGE_DIR" && rpm2cpio "$PACKAGE" | cpio -dim ./usr/lib/x64-athena/libASEP11.so)
tsudo cp -p "$PACKAGE_DIR"/usr/lib/x64-athena/libASEP11.so /usr/lib/

echo_debug "Symlinks y componentes..." # DEBUG
tsudo sh -c '
mkdir -p /usr/lib/x64-athena/
mkdir -p /Firma_Digital/LIBRERIAS/
ln -sf /usr/lib/libASEP11.so /usr/lib/x64-athena/
ln -sf /usr/lib/libASEP11.so /usr/local/lib/
ln -sf /usr/lib/libASEP11.so /Firma_Digital/LIBRERIAS/
ln -sf /usr/share/ca-certificates/trust-source/anchors /Firma_Digital/CERTIFICADOS
ln -sf /usr/lib/p11-kit-proxy.so /usr/lib/firefox/libosclientcerts.so
systemctl enable --now pcscd.socket > /dev/null
'

echo_debug "Configurando IDPClientDB" # DEBUG
tsudo sh -c "
mkdir -p /etc/Athena
echo \"<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<IDProtect>
 <TokenLibs>
  <IDProtect>
   <Cards>
    <IDProtectXF>
     <ATR type='hexBinary'>3BDC00FF8091FE1FC38073C821106600000000000000</ATR>
     <ATRMask type='hexBinary'>FFFF00FFF0FFFFFFFFFFFFFFFFF0FF00000000000000</ATRMask>
    </IDProtectXF>
   </Cards>
  </IDProtect>
  <ChipDoc>
   <Cards>
    <ChipDocEMV>
     <ATR type='hexBinary'>3BEA00008131FE450031C173C840000090007A</ATR>
     <ATRMask type='hexBinary'>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF</ATRMask>
    </ChipDocEMV>
   </Cards>
  </ChipDoc>
 </TokenLibs>
</IDProtect>\" > /etc/Athena/IDPClientDB.xml
"

echo_debug "Configurando p11-kit/modules" # DEBUG
tsudo sh -c "
mkdir -p /usr/share/p11-kit/modules
echo 'remote: |bwrap --unshare-all --dir /tmp --proc /proc --dev /dev --ro-bind /etc/Athena /etc/Athena --ro-bind /usr /usr --ro-bind /lib64 /lib64 --ro-bind /run/pcscd /run/pcscd p11-kit remote /usr/lib/libASEP11.so' > /usr/share/p11-kit/modules/firma-digital.module
"
}

install_certs() {
[ -z "$SUDO_PASSWORD" ] && return 1
[ -z "$SAVE_FILE" ] && return 1
SAVE_DIR="${SAVE_FILE%/*}"

if   [ "$ID" = "macos" ] ; then
    open "$SAVE_FILE" || return 1

elif [ "$ID" = "debian" ] ; then
    debian_install_certs || return 1

elif [ "$ID" = "fedora" ] ; then
    fedora_install_certs || return 1

elif [ "$ID" = "arch" ] ; then
    arch_install_certs || return 1

elif [ "$ID" = "centos" ] ; then
    echo || return 1

fi
}
