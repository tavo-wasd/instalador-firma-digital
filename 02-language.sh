#!/bin/sh
# False positive
# shellcheck disable=SC2034

set_lang() {
    # See: /var/lib/AccountsService/users/
    [ -e "/etc/default/locale" ] && . /etc/default/locale
    SLANG="${LANG%%_*}"
    [ -z "$SLANG" ] && SLANG="es"

    if [ "$SLANG" = "es" ] ; then
        TITLE="Instalador firma digital"
        PROMPT_WELCOME="Bienvenido al asistente de instalación de certificados para firma digial."
        PROMPT_ERR_DEPS="Error, la instalación requiere de los siguientes programas no presentes: "
        PROMPT_SERIAL="Ingrese el número serial impreso al reverso de la tarjeta."
        PROMPT_ERR_SERIAL="Error al obtener el número serial de la tarjeta, abortando."
        PROMPT_DOWNLOAD="Descargando desde Centro de Soporte Firma Digital..."
        PROMPT_DEPS_INSTALL="Instalando dependencias y otros complementos..."
        PROMPT_PASS_DEPS_INSTALL="Ingrese la contraseña del equipo para instalar los componentes"
        PROMPT_ERR_DEPS_INSTALL="Error instalando dependencias."
        PROMPT_ERR_DOWNLOAD="Error al descargar el fichero, abortando."
        PROMPT_END_SUCCESS="El instalador ha concluido."

    elif [ "$SLANG" = "en" ] ; then
        TITLE="Digital signature installer"
        PROMPT_WELCOME="Welcome to the digital signing certificate installation wizard."
        PROMPT_ERR_DEPS="Error, installation requires the following programs not present: "
        PROMPT_SERIAL="Enter the serial number printed on the back of the card."
        PROMPT_ERR_SERIAL="Error obtaining the card serial number, aborting."
        PROMPT_DOWNLOAD="Downloading from the Digital Signature Support Center..."
        PROMPT_DEPS_INSTALL="Installing dependencies and other components..."
        PROMPT_PASS_DEPS_INSTALL="Enter your computer password to install components"
        PROMPT_ERR_DEPS_INSTALL="Error installing dependencies."
        PROMPT_ERR_DOWNLOAD="Error downloading file, aborting."
        PROMPT_END_SUCCESS="The installer has completed."

    elif [ "$SLANG" = "fr" ] ; then
        TITLE="Installation de signature"
        PROMPT_WELCOME="Bienvenue dans l'assistant d'installation du certificat de signature."
        PROMPT_ERR_DEPS="Erreur, l'installation requise des programmes suivants ne présente pas: "
        PROMPT_SERIAL="Entrez le numéro de série imprimé au dos de la carte."
        PROMPT_ERR_SERIAL="Erreur d'obtention du numéro de série de la carte, abandon."
        PROMPT_DOWNLOAD="Téléchargement du fichier depuis le Centre de support des signatures numériques..."
        PROMPT_DEPS_INSTALL="Installation des dépendances et d'autres composants..."
        PROMPT_PASS_DEPS_INSTALL="Entrez le mot de passe de votre ordinateur pour installer les composants"
        PROMPT_ERR_DEPS_INSTALL="Erreur lors de l'installation des dépendances."
        PROMPT_ERR_DOWNLOAD="Erreur de téléchargement du fichier, abandon."
        PROMPT_END_SUCCESS="Le programme d'installation est terminé."

    fi
}
